{-# LANGUAGE OverloadedStrings #-}

module Lib
( installer
) where

import           Control.Monad
import           Data.Maybe
import qualified Data.Text          as T
import qualified ModifyProfile      as MP
import qualified System.Directory   as Dir
import           System.Environment
import           System.Info        as S
import qualified System.Process     as P

data OS = Win | Mac | Linux deriving (Show, Eq)

newMinecraftFolder = "conquest-reforged"
launcherProfilesJson = "launcher_profiles.json"

getMinecraftFolder :: IO (Maybe (FilePath, OS))
-- ^ Will attempt to get the filepath for the .minecraft folder
getMinecraftFolder =
  case S.os of
    "linux" -> return $ Just ("~/.minecraft/", Linux)
    "mingw32" -> do
      user <- getEnv "USERNAME"
      return $ Just ("C:\\Users\\"++ user++ "\\AppData\\Roaming\\.minecraft\\", Win)
    "darwin" -> return $ Just ("~/Library/Application Support/minecraft/", Mac)
    _ -> return Nothing

installForge path = do
  putStrLn "Checking Forge install..."
  forgeExists <- Dir.doesFileExist (path ++ "config/forge.cfg")
  unless forgeExists runForge

runForge = do
  putStrLn "Forge installation needed, please install Forge!"
  putStrLn "Launching Forge installer..."
  (_,_,_,p) <- P.createProcess (P.proc "java" ["-jar", "forgeInstaller.jar"])
  exitCode <- P.waitForProcess p
  putStr "Forge installer exited with code: "
  print exitCode

installMods modFolder = do
  putStrLn $ "Updating " ++ newMinecraftFolder ++ " folder..."
  Dir.createDirectoryIfMissing False modFolder
  putStrLn $ "Moving mods into " ++ newMinecraftFolder ++ "folder..."
  (init.init) <$> Dir.getDirectoryContents "mods" >>= mapM_ (\file ->
    Dir.copyFile ("mods/" ++ file) (modFolder ++ "/" ++ file))

addProfile modFolder profilesPath = do
  putStrLn "Adding conquest profile to launcher..."
  launchProfilesExist <- Dir.doesFileExist profilesPath
  if launchProfilesExist
    then MP.modifyProfileJSON profilesPath (T.pack modFolder)
    else do
      putStrLn "Error!"
      putStrLn $ launcherProfilesJson ++ " could not be found in your .minecraft folder!"
      putStrLn "New profile has NOT been created."

installer :: IO ()
installer = do
  folder <- getMinecraftFolder
  flip (maybe (print "Your OS is unsupported!")) folder $ \ (path, os) -> do
    let modFolder = path ++ newMinecraftFolder
    let profilesPath = path ++ launcherProfilesJson
    installForge path
    installMods modFolder
    addProfile modFolder profilesPath
