{-# LANGUAGE OverloadedStrings #-}
module ModifyProfile
( modifyProfileJSON
) where

import           ProfileData

import           Control.Applicative
import           Data.Aeson
import           Data.Aeson.Encode.Pretty (encodePretty)
import           Data.Aeson.Lens
import           Data.Aeson.Types
import           Data.ByteString          as B
import           Data.ByteString.Lazy     as BL
import           Data.HashMap.Strict      as HM
import           Data.Text                as T
import           Data.Traversable

profileName :: T.Text
profileName = "Conquest Reforged 1.9.4"

conquestProfile gdir = Profile profileName (Just gdir) Nothing (Just args) (Just True) (Just "keep the launcher open")
  where args = "-Xmx2G -XX:+UseConcMarkSweepGC -XX:+CMSIncrementalMode -XX:-UseAdaptiveSizePolicy -Xmn128M"

addProfile :: T.Text -> HM.HashMap T.Text Profile -> HM.HashMap T.Text Profile
-- ^ Adds the new Conquest Profile if it does not already exist
addProfile gdir ps = if not $ HM.member profileName ps
                     then HM.insert profileName (conquestProfile gdir) ps
                     else ps

modifyProfile :: T.Text -> ProfilesFile -> ProfilesFile
modifyProfile gdir (ProfilesFile ps sp ct ad su lv) = ProfilesFile (addProfile gdir ps) sp ct ad su lv

modifyProfileJSON path gdir = do
  jsonProfiles <- BL.readFile path
  case decode jsonProfiles :: Maybe ProfilesFile of
    (Just profiles) -> BL.writeFile path (encodePretty (modifyProfile gdir profiles))
    Nothing -> print $ "Could not read " ++ path
