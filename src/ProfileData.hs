{-# LANGUAGE DeriveAnyClass    #-}
{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}

module ProfileData where

import           Control.Applicative
import           Data.Aeson
import           Data.Aeson.Lens
import           Data.Aeson.Types
import           Data.HashMap.Strict as HM
import           Data.Text           as T
import           Data.Traversable
import           GHC.Generics
import Data.Aeson.TH

data Profile = Profile
  { name                          :: T.Text
  , gameDir                       :: Maybe T.Text
  , lastVersionId                 :: Maybe T.Text
  , javaArgs                      :: Maybe T.Text
  , useHopperCrashService         :: Maybe Bool
  , launcherVisibilityOnGameClose :: Maybe T.Text
  } deriving (Show, Generic)

instance FromJSON Profile where
  parseJSON = withObject "profile" $ \o -> do
    name <- o .: "name"
    gameDir <- optional $ o .: "gameDir"
    lastVersionId <- optional $ o .: "lastVersionId"
    javaArgs <- optional $ o .: "javaArgs"
    useHopperCrashService <- optional $ o .: "useHopperCrashService"
    launcherVisibilityOnGameClose <- optional $ o .: "launcherVisibilityOnGameClose"
    return Profile{..}

instance ToJSON Profile where
  toEncoding = genericToEncoding (Options id id True True defaultTaggedObject False)

data AuthDatabase = AuthDatabase
  { displayName    :: T.Text
  , userProperties :: Maybe [UserProperty]
  , accessToken    :: T.Text
  , userid         :: T.Text
  , uuid           :: T.Text
  , username       :: T.Text
  } deriving (Show, Generic, FromJSON)

instance ToJSON AuthDatabase where
  toEncoding = genericToEncoding (Options id id True True defaultTaggedObject False)

data UserProperty = UserProperty
  { userName  :: T.Text
  , userValue :: T.Text
  } deriving (Show)

instance FromJSON UserProperty where
  parseJSON = withObject "userproperty" $ \o -> do
    userName <- o .: "name"
    userValue <- o .: "value"
    return UserProperty {..}

instance ToJSON UserProperty where
  toJSON UserProperty{..} = object [
    "name" .= userName,
    "value" .= userValue ]

data LauncherVersion = LauncherVersion
  { launcherName   :: T.Text
  , launcherFormat :: Int
  } deriving (Show)

instance FromJSON LauncherVersion where
  parseJSON = withObject "launcherversion" $ \o -> do
    launcherName <- o .: "name"
    launcherFormat <- o.: "format"
    return LauncherVersion{..}

instance ToJSON LauncherVersion where
  toJSON LauncherVersion{..} = object [
    "name" .= launcherName,
    "format" .= launcherFormat ]

data ProfilesFile = ProfilesFile
  { profiles               :: HM.HashMap T.Text Profile
  , selectedProfile        :: T.Text
  , clientToken            :: T.Text
  , authenticationDatabase :: HM.HashMap T.Text AuthDatabase
  , selectedUser           :: T.Text
  , launcherVersion        :: LauncherVersion
  } deriving (Show, Generic, FromJSON)


instance ToJSON ProfilesFile where
  toEncoding = genericToEncoding (Options id id True True defaultTaggedObject False)
